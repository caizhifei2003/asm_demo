#include <cstdlib>
#include <cstdio>
#include <ctime>

extern "C" int LOC_FIB_NUM;

extern "C" int loc_add(int a, int b);
extern "C" int loc_sub(int a, int b);
extern "C" int loc_add_p(int a, int b, int *ret);
extern "C" int loc_and(int a, int b);
extern "C" int loc_const(int i, int *a, int *b, int *c);

void __cdecl begin_test();
void __cdecl end_test();
void __cdecl test_add();
void __cdecl test_sub();
void __cdecl test_add_p();
void __cdecl test_and();
void __cdecl test_const();

int main() {
	begin_test();

	test_add();
	test_sub();
	test_add_p();
	test_and();
	test_const();

	end_test();
}

void begin_test() {
	srand(time(NULL));
}

void end_test() {

}

void test_add() {
	int a = rand(), b = rand();
	int ret = loc_add(a, b);
	printf("%d + %d = %d\r\n", a, b, ret);
}

void test_sub() {
	int a = rand(), b = rand();
	int ret = loc_sub(a, b);
	printf("%d - %d = %d\r\n", a, b, ret);
}

void test_add_p() {
	int a = rand(), b = rand(), ret = 0;
	loc_add_p(a, b, &ret);
	printf("%d + %d = %d\r\n", a, b, ret);
}

void test_and() {
	int a = rand(), b = rand();
	int ret = loc_and(a, b);
	printf("%d and %d = %d\r\n", a, b, ret);
}

void test_const() {
	for (int i = -1; i <= LOC_FIB_NUM; ++i) {
		int a = 0, b = 0, c = 0;
		int ret = loc_const(i, &a, &b, &c);
		printf("LOC_FIB[%d]: %d, %d, %d, %d\r\n", i, ret, a, b, c);
	}
}