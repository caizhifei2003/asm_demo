.MODEL flat, c
.CONST
LOC_FIB	DD 9, 1, 1, 2, 3, 5, 8, 13
		DD 21, 34, 55, 89, 144, 233, 377, 610
LOC_FIB_NUM DD ($ - LOC_FIB) / sizeof DWORD
PUBLIC	LOC_FIB_NUM

.CODE

loc_add PROC

	push	ebp
	mov		ebp, esp

	mov		eax, [ebp + 8]
	add		eax, [ebp + 12]

	pop		ebp
	ret

loc_add ENDP

loc_sub PROC

	push	ebp
	mov		ebp, esp

	mov		eax, [ebp + 8]
	sub		eax, [ebp + 12]

	pop		ebp
	ret

loc_sub ENDP

loc_add_p PROC

	push	ebp
	mov		ebp, esp
	push	ebx

	mov		eax, [ebp + 8]
	add		eax, [ebp + 12]

	mov		ebx, [ebp + 16]
	mov		[ebx], eax

	pop		ebx
	pop		ebp
	ret

loc_add_p ENDP

loc_and PROC

	push	ebp
	mov		ebp, esp

	mov		eax, [ebp + 8]
	mov		ecx, [ebp + 12]
	and		eax, ecx

	pop		ebp
	ret

loc_and ENDP

loc_const PROC

	push	ebp
	mov		ebp, esp
	push	ebx
	push	esi
	push	edi

	xor		eax, eax
	mov		ecx, [ebp + 8]
	cmp		ecx, 0
	jl		InvalidIndex
	cmp		ecx, [LOC_FIB_NUM]
	jge		InvalidIndex

	mov		ebx, offset LOC_FIB
	mov		esi, [ebp + 8]
	shl		esi, 2
	add		ebx, esi
	mov		eax, [ebx]
	mov		edi, [ebp + 12]
	mov		[edi], eax

	mov		esi, [ebp + 8]
	shl		esi, 2
	mov		eax, [esi + LOC_FIB]
	mov		edi, [ebp + 16]
	mov		[edi], eax

	mov		ebx, offset LOC_FIB
	mov		esi, [ebp + 8]
	shl		esi, 2
	mov		eax, [ebx + esi]
	mov		edi, [ebp + 20]
	mov		[edi], eax

	mov		ebx, offset LOC_FIB
	mov		esi, [ebp + 8]
	mov		eax, [ebx + esi * 4]
	mov		edi, [ebp + 24]
	mov		[edi], eax

	mov		eax, 1

InvalidIndex:
	pop		edi
	pop		esi
	pop		ebx
	pop		ebp
	ret

loc_const ENDP

END